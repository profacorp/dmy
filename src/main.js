import { createApp } from "vue";
import VueGtag from "vue-gtag";
import App from "./App.vue";

const app = createApp(App);
app.use(VueGtag, {
  config: { id: "G-VF61E3VRZ0" },
});
app.mount("#app");
